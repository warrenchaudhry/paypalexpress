class OrdersController < ApplicationController
  include Yukon
  # Step 1 : SetExpressCheckout
  def express
    # price (in cents), buyer_ip, return_url, cancel_return_url, notify_url (optional)
    @payment_processor = PaymentProcessor.new('sell_1342421142_biz@gmail.com')
    response = @payment_processor.set_express_checkout(current_cart.build_order.price_in_cents,
                                                     request.remote_ip,
                                                     new_order_url,
                                                     products_url,
                                                     'bugs')
                                                     
    # Redirect to Paypal URL : args - response
    redirect_to @payment_processor.redirect_url_for(response)
  end
  
  # This is the return_url. After Paypal interaction this is executed.
  def new
    @order = current_cart.build_order
    # Step 2 : GetExpressCheckoutDetails  : args - token
    @payment_processor = PaymentProcessor.new('sell_1342421142_biz@gmail.com')
    details = @payment_processor.get_express_checkout_details(params[:token])
    @order.details = details.params
    @order.ip_address = request.remote_ip

    if @order.save
      # Step 3 : DoExpressCheckoutPayment
      response = @payment_processor.do_express_checkout_payment(@order.price_in_cents, 
                                                                request.remote_ip,
                                                                params[:token],
                                                                params[:PayerID])
      
      if response.success?
        @order.payments.create!(action: "purchase", amount: @order.price_in_cents, response: response)
        # Checkout complete. 
        clear_cart_session
        
        render action: "success"
      else
        render action: "failure"
      end
    else
      render action: 'new'
    end
  end

  private
    
  def clear_cart_session
    session[:cart_id] = nil
  end
end