class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer  :order_id
      t.string   :action
      t.integer  :amount
      t.boolean  :success
      t.string   :authorization
      t.string   :message
      t.text     :details
      
      t.timestamps null: false
    end
  end
end